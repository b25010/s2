package com.zuitt.example;
import java.util.Scanner;

import static java.lang.System.out;
public class LeapYear {
    public static void main(String[] args){
        Scanner leapYear = new Scanner(System.in);

        out.println("Enter year: ");
        int userInput = leapYear.nextInt();

        if(userInput % 4 == 0 && userInput % 100 != 0 || userInput % 400 == 0){
            out.println(userInput + " is a Leap Year!");
        }else{
            out.println(userInput + " is not a Leap Year!");
        }

    }
}
