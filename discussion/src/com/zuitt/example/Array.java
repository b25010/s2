package com.zuitt.example;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static java.lang.System.out;

public class Array {
    //[SECTION] Java Collection - are single unit of objects.
        // useful for manipulating relevant pieces of data that can be used in different situation, more commonly with loops.

    public static void main(String[] args){

        // [SECTION]
            // In Java, arrays are containers of values of the same type iven a predefined amount values.
            // Java arrays are more rigid; Once the size and data type are defined, they can no longer be changed.

        // Array Declaration
            // Syntax:
            // dataType[] identifier = new dataType[numOfElements];
                // "[]" indicates that the data type should be able to hold multiple values.
                // "new" keyword is for the non-primitive data types to tell Java to create the said variable.
                // the values of the array is initialized either with 0 or null.

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
//        intArray[5] = 100;  -- out of bound exception/error

        // this will return/print the memory address of the array
        // out.println(intArray);

        // To print the intArray, we need to import the Arrays class and use the .toString() method to convert the array into string when we print it into the terminal.
        out.println(Arrays.toString(intArray));

        // Declaration with Initialization
            // Syntax:
                // dataType[] identifier = {elementA, elementB, elementC, ...};
            // the compiler automatically specifies the size by counting the number of the elements in the array;
        String[] names = {"John", "Jane", "Joe"};
        // names[3] = "Joey"; --- out of bounds
        out.println(Arrays.toString(names));

        // Sample java Array methods
        // Sort

        Arrays.sort(intArray);
        out.println("Order of items after the sort method: " + Arrays.toString(intArray));

        // Multidimensional Arrays
            // a two-dimensional  arrays can be described by two lengths nested within each other, like a matrix

        String[][] classroom = new String[3][3];

        // First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        // Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";
        // Third Row
        classroom[2][0] = "Mickey Mouse";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        // out.println(Arrays.toString(classroom));
        // we use the deepToString() method in accessing values for multidimensional arrays.
        out.println(Arrays.deepToString(classroom));

        // In Java, the size of the array cannot be modified. Id there is a need to add or remove elements, new arrays must be created.

        // [SECTION] ArrayLists
            // are resizable arrays, wherein elements can be added or removed whenever it is needed.

        // Syntax:
            // ArrayList<T> identifier = new ArrayList<T>();
            // "T" is used to specify that the list can only have one type of object in a collection.
            // ArrayList cannot hold primitive data types, "java wrapper class" provides a way to use these types as data object.
                // in short, Object version of primitive data types  with method.

        // Declaring an ArrayList
        // ArrayList<int> numbers = new ArrayList<int>(); - type argument cannot be of primitive type.
        ArrayList<Integer> numbers = new ArrayList<>();

        // Declaring an ArrayList then initializing
        // ArrayList<String> students = new ArrayList<>();

        // Declaring an ArrayList with values
         ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane", "Mike"));

        // Add elements
        students.add("John");
        students.add("Paul");
        out.println(students);

        // Access an element/s
        out.println(students.get(0));
        out.println(students.get(3));

        // Add an element to a specific index.
        students.add(0, "Joey");
        out.println(students);

        // Updating an element
        students.set(0, "George");
        out.println(students);

        // Removing a specific element
        students.remove(1);
        out.println(students);

        // Removing all elements
        students.clear();
        out.println(students);

        // Getting the arrayList size.
        out.println((students.size()));


        // [SECTION] Hashmaps
            // most objects in Java are defined and are instantiation of classes that contain a set of properties and methods.

            // Syntax:
            // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();

        // Declaring HashMaps
        // HashMap<String, String> jobPosition = new HashMap<String, String>();

        // Declaring HashMaps with initialization
        HashMap<String, String> jobPosition = new HashMap<String, String>() {
            {
                put("Teacher", "John");
                //put("Artist", "Jane");
            }
        };
        out.println(jobPosition);

        // Add elements
        // hashMapsName.put(<fieldName>, <value>);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        // jobPosition.put("Student", "Jane"); - the last one will be overridden, whenever the same key is used
        out.println(jobPosition);

        // Accessing element(caseSensitive) - we use the field name because they are unique.
        // hashMapName.get("fieldName");
        // wrong fieldName will return a null value.
        out.println(jobPosition.get("Student"));

        // Updating the value
        // hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Student", "Brandon Smith");
        out.println(jobPosition);

        // Removing an element
        // hashMapName.remove(key);
        jobPosition.remove("Dreamer");
        out.println(jobPosition);

        // Remove all elements
        // hashMapName.clear();
        jobPosition.clear();
        out.println(jobPosition);

    }
}
