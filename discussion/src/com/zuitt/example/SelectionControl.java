package com.zuitt.example;
import java.util.Scanner;

import static java.lang.System.out;

public class SelectionControl {

    public static void main(String[] args){
        // [SECTION] Java Operators
        // Arithmetic --> +, -, *, /, %
        // Comparison --> >, <, >=, <=, ==, !=
        // Logical --> &&, ||, !
        // Assignment --> =
            // 'a = a + 1' is same with 'a += 1';

        // [SECTION] Control Structures in Java

        // Syntax:
            /*
            * if(condition){ code block }
            *
            * else{ code block }
            * */
        int num1 = 36;
        if(num1 % 5 == 0){
            out.println(num1 + " is divisible by 5.");
        }else{
            out.println(num1 + " is not divisible by five.");
        }

        // [SECTION] Short Circuiting

        int x = 15;
        int y = 1;

        if(y != 0 && x/y == 0){
            out.println("Result is: " + x/y);
        }else{
            out.println("This will only run because of short circuiting.");
        }

        // [SECTION] Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        out.println(result);

        // [SECTION] Switch Object
        Scanner numberScanner = new Scanner(System.in);
        out.println("Enter a number");
        int directionValue = numberScanner.nextInt();

        switch (directionValue) {
            case 1:
                out.println("North");
                break;
            case 2:
                out.println("South");
                break;
            case 3:
                out.println("West");
                break;
            case 4:
                out.println("East");
                break;
            default:
                out.println("Invalid input. Please select from 1-4.");
                break;
        }

    }
}
