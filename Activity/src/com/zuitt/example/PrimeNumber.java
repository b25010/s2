package com.zuitt.example;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import static java.lang.System.out;

public class PrimeNumber {
    public static void main(String[] args){
        int[] primeArray = new int[5];
        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;
        out.println("The first prime number is: " + primeArray[0]);


        ArrayList<String> friendsName = new ArrayList<>();
        friendsName.add("John");
        friendsName.add("Jane");
        friendsName.add("Chloe");
        friendsName.add("Zoey");
        out.println("My friends are: " + friendsName);


        HashMap<String, Integer> products = new HashMap<String, Integer>() {
            {
                put("Toothpaste", 15);
                put("Toothbrush", 20);
                put("Soap", 12);
            }
        };
        out.println("Our current inventory consists of: " + products);

    }
}
